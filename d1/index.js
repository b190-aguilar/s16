console.log("Hello World");
/*
	JS can also command our browsers to perform different arithmetic operations just like how mathematics work
*/

// ARITHMETIC OPERATORS SECTION

/*
	basic operators
	+ Addition
	- Subtraction
	* Multiplication
	/ Division
	% Modulo (returns remainder in division)
*/

// creation of variables to be used in mathematical operations
let x = 1397;
let y = 7831;
console.log("x = " + x);
console.log("y = " + y);


let sum = x + y;
console.log(sum);


let difference = x-y;
console.log(difference);

let product = x*y;
console.log(product);

let quotient = x/y;
console.log(quotient);


let remainder = y % x;
console.log(remainder);



// Assignment operator
	// used to assign a value to a variable
let assignmentNumber = 8;


// assignmentNumber = assignmentNumber + 2;
// SHORT HAND  for operation assignment +=
assignmentNumber += 2;
console.log(assignmentNumber);

assignmentNumber -= 2;
console.log(assignmentNumber);

assignmentNumber *= 2;
console.log(assignmentNumber);

assignmentNumber /= 2;
console.log(assignmentNumber);



// Multiple Operators and Parenthesis
	// JS Operators follow the PEMDAS Rule


/*
code below is computed as:
	3*4 = 12 
	12/5 = 2.4
	1+2 = 3
	3 - 2.4 = 0.6 
*/	
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);


let pemdas = 1 + (2-3) * 4 / 5;
console.log(pemdas);

/*
	2-3 = -1
	-1 * 4 = -4
	-4 / 5 = -0.8
	1 + (-0.8) = 0.2 (0.19999999999999996)
*/


pemdas = (1 + (2-3)) * (4/5);
console.log(pemdas);


// Increment and Decrement Section
// assigning a value to a variable to be used in increment and decrement section
let z = 1;

/*
	INCREMENT ++ - adding 1 to the value of the variable whether before or after the assigning of value.
		pre-increment - adding 1 to the value BEFORE it is assigned to the variable
		post-increment - addign 1 to the value AFTER it is assigned to the variable
*/

// PRE-INCREMENT
let increment = ++z;
// the value of z is added by a value of 1 before returning the value and storing it inside the z variable.
console.log("Result of pre-increment: " + increment);
// the value of z was also incread by 1 event though we didnt explicity specifit any value reassignment
console.log("Result of pre-increment: " + z);


// POST-INCREMENT
increment = z++;
// the value of z is at 2 before it was incremented
console.log("Result of post-increment: " + increment);
// the value of z was increased again reassigning the value to 3
console.log("Result of post-increment: " + z);


/*
	DECREMENT -- 
*/

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);


// Type COERCION
/*
	is the automatic or implicit conversion of values from one data type to another
	this happens when operations are performed on different data types that would normally not be possible and yield irregular results
	values are automatically assigned/converted from one data type to another in order to resolve the operation.
*/

let numbA = '10';
let numbB = 12;
let coercion = numbA + numbB;
console.log(coercion);

/*
	num + string = string
*/


/*
	try to have coercion for the following:

	- number data + number data = number/integer
	- boolean + number = number/integer
	- boolean + string = string

*/

let booleanX = true;
let numberX = 25;
let numberY = 35;

// non-coercion if same data types
let noncoercion = numberX + numberY;
console.log(noncoercion);
console.log(typeof noncoercion)

// boolean = binary (True = 1, False = 0)
/*
	results into a number data type
*/
let numbE = booleanX + numberX;
console.log(numbE);
console.log(typeof numbE);

let varA = booleanX + "String";
console.log(varA);
console.log(typeof varA);


//	Comparison Operators

/*
	Equality Operator 
		checks if the operands are equal or have the same content
		attempts to convert and compare operands of different data types
*/
console.log(1 == 1);
// returns boolean value: TRUE

console.log(1 == 2);
// returns boolean value: FALSE


console.log(1 == "1");
console.log(1 == true);
console.log("juan" == "juan");
let juan = "juan";
console.log("juan" == juan);


/*
	Inequality Operator
		checks if the operands are not equal or does not have the same content
		attempts to convert and compare operands of different data types
*/
console.log("-----");	

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log(0 != false);
console.log("juan" != "juan");
console.log("juan" != juan);


//	Strict Equality/inequality Operators
	/*
		strict when it comes to data type comparisons
		JS is a loosely typed language, meaning that the values of different data type can be stored inside a variable


		strict operators are better to be used in most cases to ensure that the data types provided are correct.
	*/
console.log("-----");	

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === "1");
console.log(0 === false);
console.log("juan" === "juan");
console.log("juan" === juan);

console.log("-----");	

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== "1");
console.log(0 !== false);
console.log("juan" !== "juan");
console.log("juan" !== juan);


// Relational Operators
console.log("-----");	

/*
	returns Boolean value based on the assessment of the operands.
*/

let a = 50;
let b = 65;
// GT
let greaterThan = a > b;
// LT
let lessThan = a < b;
// GTE
let greaterThanOrEqualTo = a>=b;
// LTE
let lessThanOrEqualTo = a<=b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

// true - product of a forced coercion to change the string into a number data type
let numStr = "30";
console.log(a > numStr);

// since the string is not numeric, the string was converted into a number and it resulted into NaN (Not a Number)
let str = "twenty";
console.log(b >= str);


// Logical Operators
	/*
		checking whether the values of two or more variables are True or False
	*/

let isLegalAge = true;
let isRegistered = false;

// AND Operator "&&"

/*
	1 			2 			Result
	True 		True 		True
	False 		True 		False
	True 		False		False
	False 		False 		False
*/

let allRequirements = isLegalAge && isRegistered;
console.log("Result of AND Operator " + allRequirements);


// OR Operator "||"

/*
	1 			2 			Result
	True 		True 		True
	False 		True 		True
	True 		False		True
	False 		False 		False
*/
let someRequirementsMet = isLegalAge || isRegistered
console.log("Result of OR Operator " + someRequirementsMet);


// NOT Operator "!"
	// returns the opposite of the value

/*
	1 			Result
	True 		False
	False 		True
*/	

let someRequirementsNotMet = !isRegistered; 
console.log("Result of NOT Operator: " + someRequirementsNotMet);
